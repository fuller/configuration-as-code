# Codeberg - Configuration

Codeberg's Configuration (as Code, using Ansible).
This is an ongoing effort:
Starting with our own hardware server (2021),
all configs should be public and documented.

However, some previous systems are still not migrated
and thus inaccessible (WIP).
Since effectively all systems will be migrated from our rented VPS
sooner or later, this situation will improve over time.

Exceptions to this:

- quick n dirty testing environments don't need to be configured publicly
- not using Ansible for configuring the inside of containers is possible, as long as the configuration is otherwise accessible and the setup documented

Some further notes on the state of this repo:  

> Sadly, this project didn't go too well since the beginning.
This framework was rather complex to get right,
and although it's working *for us* now,
some parts like "getting started easily" were lost in the trouble again.
We're looking forward to your contribution to this project,
be it patches or inspiration,
also regarding workflow and management.
Thank you for understanding!

## [Please have a look at our new Wiki for more information!](https://codeberg.org/Codeberg-Infrastructure/configuration-as-code/wiki/%F0%9F%8F%A0-Welcome)

## Relevant Documentation

<!-- - [Concepts of Ansible](https://momar.de/blog/concepts-of-ansible)-->
<!-- - [Concepts of Docker](https://momar.de/blog/concepts-of-docker)-->

- [Ansible Core Docs][1] (server setup)
- [Ansible Module Index][2] (full documentation of all modules)
- [Docker Compose Specification][3] (everything for service deployment)
- [Docker Compose Deploy Specification][4] (everything for services in clusters)
- [Traefik Docs][5] (our reverse proxy)

[1]: https://docs.ansible.com/ansible/latest/index.html
[2]: https://docs.ansible.com/ansible/latest/collections/index_module.html
[3]: https://github.com/compose-spec/compose-spec/blob/master/spec.md
[4]: https://github.com/compose-spec/compose-spec/blob/master/deploy.md
[5]: https://doc.traefik.io/traefik/

## Local Testing VM

> ⚠️ **Warning: the local testing VM is currently not really tested and
> unsupported, and will be .**

A local testing VM is provided - the required rependencies are
`virtinst libvirt-clients libvirt-daemon-driver-qemu`.

To set up the VM, run `./local-testing vm create` (no need to sudo, everything's
running in user mode).
After the installation is complete and the login prompt is shown, press
`Ctrl + ]` to detach.
You can then connect to the VM via SSH on `127.0.0.1:3222`, using
`./local-testing ssh` - the default username/password combination is
`codeberg-setup` / `codeberg`.

**Important note:** due to QEMU's usermode networking limitations, it's not
possible to reach the VM under an IP address - to access the VM's internal
network, you can use the SOCKS proxy exposed by `./local-testing ssh` on
`127.0.0.1:3280`.

**Important note:** adding the SSH key to the `known_hosts` file is required for
Ansible to run when using password authentication (like with the VM). You can
set this up by running `./local-testing ssh -k`.

## Server prerequisites

- Working Debian 11
- SSH with working public key authentication, using a hardware security key
- sudo for the setup user
- SSH daemon running on port 19198

## Ansible Dependencies

Ansible completely runs on your local computer, no setup on the server is
required. The following Ansible prerequisites are required on your machine:

Debian 11 and Ubuntu 21.04 already include Ansible 2.10:

```bash
apt install sshpass ansible
```

For older distributions you need to use pip to install Ansible:

```bash
apt install sshpass python3-pip
pip3 install ansible # WE NEED AT LEAST VERSION 2.10!!!
```

Regardless of your distribution we need some extra packages from ansible-galaxy:

```bash
ansible-galaxy install -r requirements.yml
```

**Further instructions on how to actually deploy a playbook can be found
[in the wiki](https://codeberg.org/Codeberg-Infrastructure/configuration-as-code/wiki/Deployment-Process).**

<!--To deploy everything required for a host system to your local testing VM,
run `./local-testing deploy`, basically a shorthand for the ansible command
below.

To deploy to staging (or respectively production), run
`ansible-playbook -Kbi inventories/staging/hosts playbook.yml`.

To add secrets (if you have the permission to access them), see the `secrets`
submodule. You can fetch it with `git submodule init && git submodule update`.

**Important note:** Make sure you have set up a user with a password and a
hardware-backed SSH key in `roles/admin_users/vars/main.yml`, or you won't have
access to the system.-->

## Role Documentation

**All roles & variables are documented in the
[ROLES-AND-VARIABLES.md](ROLES-AND-VARIABLES.md) file.**
