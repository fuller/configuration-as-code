#####################################
## Global Configuration & Defaults ##
#####################################

global
  {% if not haproxy_debug %}# {% endif %}log stderr local7

  # generated 2021-06-05, Mozilla Guideline v5.6, HAProxy 2.1, OpenSSL 1.1.1d, intermediate configuration
  # https://ssl-config.mozilla.org/#server=haproxy&version=2.1&config=intermediate&openssl=1.1.1d&guideline=5.6
  ssl-default-bind-ciphers ECDHE-ECDSA-AES128-GCM-SHA256:ECDHE-RSA-AES128-GCM-SHA256:ECDHE-ECDSA-AES256-GCM-SHA384:ECDHE-RSA-AES256-GCM-SHA384:ECDHE-ECDSA-CHACHA20-POLY1305:ECDHE-RSA-CHACHA20-POLY1305:DHE-RSA-AES128-GCM-SHA256:DHE-RSA-AES256-GCM-SHA384
  ssl-default-bind-ciphersuites TLS_AES_128_GCM_SHA256:TLS_AES_256_GCM_SHA384:TLS_CHACHA20_POLY1305_SHA256
  ssl-default-bind-options prefer-client-ciphers no-sslv3 no-tlsv10 no-tlsv11 no-tls-tickets

  ssl-default-server-ciphers ECDHE-ECDSA-AES128-GCM-SHA256:ECDHE-RSA-AES128-GCM-SHA256:ECDHE-ECDSA-AES256-GCM-SHA384:ECDHE-RSA-AES256-GCM-SHA384:ECDHE-ECDSA-CHACHA20-POLY1305:ECDHE-RSA-CHACHA20-POLY1305:DHE-RSA-AES128-GCM-SHA256:DHE-RSA-AES256-GCM-SHA384
  ssl-default-server-ciphersuites TLS_AES_128_GCM_SHA256:TLS_AES_256_GCM_SHA384:TLS_CHACHA20_POLY1305_SHA256
  ssl-default-server-options no-sslv3 no-tlsv10 no-tlsv11 no-tls-tickets

  # curl https://ssl-config.mozilla.org/ffdhe2048.txt > /path/to/dhparam
  ssl-dh-param-file /etc/ssl/dhparam.pem

defaults
  log global
  timeout connect 30000
  timeout check 300000
  timeout client 300000
  timeout server 300000
  default-server init-addr last,libc,none

############################################################################
## Frontends: HTTP; HTTPS → HTTPS SNI-based; HTTPS → HTTP(S) header-based ##
############################################################################

frontend http_redirect_frontend
  # HTTP backend to redirect everything to HTTPS
  bind :::80 v4v6
  mode http
  http-request redirect scheme https

frontend https_sni_frontend
  # TCP backend to forward to HTTPS backends based on SNI
  bind :::443 v4v6
  mode tcp

  # Wait up to 5s for a SNI header & only accept TLS connections
  tcp-request inspect-delay 5s
  tcp-request content capture req.ssl_sni len 255

  # Log SNI & corresponding backend
  {% if not haproxy_debug %}# {% endif %}log-format "%ci:%cp -> %[capture.req.hdr(0)] @ %f (%fi:%fp) -> %b (%bi:%bp)"
  {% if not haproxy_debug %}# {% endif %}tcp-request content accept if { req.ssl_hello_type 1 }

  ###################################################
  ## Rules: forward to HTTPS(S) header-based rules ##
  ###################################################
  acl use_http_backend req.ssl_sni -i "{{ main_domain }}"
  acl use_http_backend req.ssl_sni -i "www.{{ main_domain }}"
  acl use_http_backend req.ssl_sni -i "about.{{ main_domain }}"
  acl use_http_backend req.ssl_sni -i "join.{{ main_domain }}"
  acl use_http_backend req.ssl_sni -i "polls.{{ main_domain }}"
  acl use_http_backend req.ssl_sni -i "ci.{{ main_domain }}"
  use_backend https_termination_backend if use_http_backend

  ############################
  ## Rules: HTTPS SNI-based ##
  ############################
  # use_backend xyz_backend if { req.ssl_sni -i "xyz" }
  default_backend pages_backend

frontend https_termination_frontend
  # Terminate TLS for HTTP backends
  bind /tmp/haproxy-tls-termination.sock accept-proxy ssl strict-sni alpn h2,http/1.1 crt /etc/ssl/private/haproxy/
  mode http

  ############################################
  ## Mitigations against malicious requests ##
  ############################################

  # Block known bad bot UAs
  tcp-request inspect-delay 15s
  acl bad-bot hdr_sub(user-agent) -f /etc/haproxy/forbidden-ua-list.txt
  tcp-request content reject if bad-bot

  # Send "429 Too Many Requests" for signup page
  http-request track-sc0 src table per_ip_rates if { path /user/sing_up }
  http-request deny deny_status 429 if { sc_http_req_rate(0) gt 100 }

  ##############################
  ## General request settings ##
  ##############################

  # Enable compression for backends which don't already do it
  compression algo gzip
  compression type application/javascript application/json image/svg+xml text/css text/html text/javascript text/plain text/xml

  # Force HTTPS through HSTS (63072000 seconds)
  http-response set-header Strict-Transport-Security "max-age=63072000; includeSubDomains; preload"

  # Opt out of FLoC explicitly
  http-response set-header Permissions-Policy "interest-cohort=()"

  # Only allow secure cookies, and don't allow cookies for CORS requests
  http-response replace-header Set-Cookie (.*) "\1; Secure; SameSite=Lax"

  # Cache avatars for 6 hours
  http-response set-header Cache-Control "private, max-age=21600" if { capture.req.uri -m beg /avatars/ }

  # Disable embedding in external frames
  http-response set-header X-Frame-Options "sameorigin"

  # Force Content-Type header to be respected, so e.g. raw links can't be used in <script> tags
  http-response set-header X-Content-Type-Options "nosniff"

  # Add CSP to avoid XSS attacks
  # Currently in Report-Only mode to detect if it theoretically would work
  # You can find a line-wrapped version (using Jinja2 comments) in the ansible template of this file
  http-response set-header Content-Security-Policy-Report-Only "{#
    #}default-src data: 'self' https://*.codeberg.org https://codeberg.org;{#
    #} script-src 'self' https://*.codeberg.org https://codeberg.org;{#
    #} style-src data: 'self' 'unsafe-inline' https://*.codeberg.org https://codeberg.org;{#
    #} img-src *;{#
    #} media-src *;{#
    #} object-src 'none';{#
    #} report-uri https://codeberg.org/.well-known/csp-report"

  # Log Host header & corresponding backend
  {% if not haproxy_debug %}# {% endif %}http-request capture req.hdr(Host) len 255
  {% if not haproxy_debug %}# {% endif %}log-format "%ci:%cp -> %[capture.req.hdr(0)] @ %f (%fi:%fp) -> %b (%bi:%bp)"

  ##################################
  ## Rules: HTTPS(S) header-based ##
  ##################################

  redirect prefix https://{{ main_domain }} code 301 if { hdr(host) -i www.{{ main_domain }} }
  redirect prefix https://about.{{ main_domain }} code 301 if { hdr(host) -i join.{{ main_domain }} }

  #use_backend csp_backend if { path -i /.well-known/csp-report }
  use_backend gitea_backend if { hdr(host) -i {{ main_domain }} }
  #use_backend about_backend if { hdr(host) -i about.{{ main_domain }} }
  #use_backend polls_backend if { hdr(host) -i polls.{{ main_domain }} }
  use_backend ci_backend if { hdr(host) -i ci.{{ main_domain }} }

  default_backend pages_backend_http

backend https_termination_backend
  # Redirect to the terminating HTTPS frontend for all HTTP backends
  server https_termination_server /tmp/haproxy-tls-termination.sock send-proxy-v2-ssl-cn
  mode tcp

backend per_ip_rates
  stick-table type ip size 1m expire 15m store http_req_rate(15m)

###############################
## Backends: HTTPS SNI-based ##
###############################

backend pages_backend
  # Pages server is a HTTP backend that uses its own certificates for custom domains
  server pages_server pages.lxc.local:443 no-check
  mode tcp

backend pages_backend_http
  # When reusing an HTTP/2 connection, HAProxy's HTTP server is already active, so we need to have a HTTP backend as well
  # This happens e.g. when visiting docs.codeberg.org (a pages site) after visiting ci.codeberg.org (a site handled by HAProxy)
  server pages_server_http pages.lxc.local:443 no-check ssl verify none sni req.hdr(host)
  mode http

####################################
## Backends: HTTP(S) header-based ##
####################################

backend gitea_backend
  errorfile 502 /etc/haproxy/error-pages/502.http
  errorfile 503 /etc/haproxy/error-pages/502.http
  option forwardfor
  server gitea_server gitea.lxc.local:80 no-check
  mode http

backend ci_backend
  errorfile 502 /etc/haproxy/error-pages/502.http
  errorfile 503 /etc/haproxy/error-pages/502.http
  option forwardfor
  server ci_server ci.lxc.local:8000 no-check
  mode http

# TODO: move the errorfile & forwardfor options to a default section?
