- name: "Configure haproxy"
  ansible.builtin.template:
    src: haproxy.cfg
    dest: /etc/haproxy/haproxy.cfg
    owner: root
    group: root
    mode: 'u=rw,g=r,o=r'
  notify: systemctl restart haproxy.service

- name: "Make sure the haproxy error page directory exists"
  ansible.builtin.file:
    path: /etc/haproxy/error-pages
    state: directory
    owner: root
    group: root
    mode: 'u=rwx,g=rx,o=rx'

- name: "Copy custom error page"
  ansible.builtin.copy:
    src: 502.http
    dest: /etc/haproxy/error-pages/502.http
    owner: root
    group: root
    mode: 'u=rw,g=r,o=r'

- name: "Copy list of forbidden user agents"
  ansible.builtin.copy:
    src: forbidden-ua-list.txt
    dest: /etc/haproxy/forbidden-ua-list.txt
    owner: root
    group: root
    mode: 'u=rw,g=r,o=r'

- name: "Generate diffie-hellman parameters (this can take a while)"
  ansible.builtin.command:
    cmd: openssl dhparam -out /etc/ssl/dhparam.pem 2048
    creates: /etc/ssl/dhparam.pem

- name: "Make sure the haproxy cert directory exists"
  ansible.builtin.file:
    path: /etc/ssl/private/haproxy
    state: directory
    mode: "0700"

- name: "Generate self-signed certificate for default domains (for debugging, when acme_method is undefined)"
  ansible.builtin.shell:
    cmd: |
      printf '%s\n[SAN]\nsubjectAltName=DNS:codeberg.org' "$(cat /etc/ssl/openssl.cnf)" | \
      openssl req \
          -newkey ec -pkeyopt ec_paramgen_curve:secp384r1 -x509 -nodes \
          -keyout /etc/ssl/private/haproxy/self-signed.crt.key \
          -new -out /etc/ssl/private/haproxy/self-signed.crt \
          -subj '/CN=*.{{ main_domain }}' \
          -reqexts SAN -extensions SAN \
          -config - \
          -sha256 \
          -days 3650
    creates: /etc/ssl/private/haproxy/self-signed.crt
  when: acme_method is undefined

- name: "Copy lego-wrapper script for easier configuration"
  ansible.builtin.copy:
    src: lego-wrapper
    dest: /usr/local/bin/lego-wrapper
    mode: "0755"

- name: "Add defaults for lego certificates"
  ansible.builtin.template:
    src: lego-default
    dest: /etc/default/lego
    mode: "0600"
  register: lego_default
  when: acme_method is defined

- name: "Update certificate for default domains"
  ansible.builtin.shell:
    # remove /etc/default/lego to make sure this step runs in the next ansible deployment
    cmd: lego-wrapper --default-domains run || { rm /etc/default/lego; exit 1; }
  when: acme_method is defined and lego_default.changed

- name: "Configure cronjob for ACME/Let's Encrypt for default domains"
  ansible.builtin.cron:
    name: "lego-renew-default-domains"
    minute: "0"
    hour: "0"
    job: "/usr/local/bin/lego-wrapper --default-domains renew"
  when: acme_method is defined
