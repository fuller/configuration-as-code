#!/bin/sh
set -e
cd "$(dirname "$(readlink -f "$0")")"
cd preseed.d

build_preseed() {
  # $1 - environment file
  cat=$(which cat)
  envsubst=$(which envsubst)
  if [ -z "$cat" ] || [ -z "$envsubst" ]; then
    echo "ERROR: cat & envsubst are required to run this script." >&2
    return 1
  fi

  #env -i --
  sh -c 'set -a && . "$1/$2" && "$3" "$1/template.cfg" | "$4"' -- "$(readlink -f "$(pwd)")" "$1" "$cat" "$envsubst"
}

mkdir -p ../preseed.compiled
rm -f ../preseed.compiled/*
for system in *.env; do
  echo "Generating $system"
  build_preseed "$system" > "../preseed.compiled/$(echo "$system" | sed 's/\.env$/.cfg/')"
done

# Update checksum for local testing VM
sed -i 's@preseed/file/checksum=\S*@preseed/file/checksum='"$(md5sum ../preseed.compiled/local.codeberg-test.org.cfg | cut -d ' ' -f 1)"'@g' ../../local-testing
