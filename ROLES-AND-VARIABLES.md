# General variables

- `main_domain`: the main domain for the deployment, defaults to `codeberg.org`

## Roles

### `generic_base`

Setup of a new server or LXC container, internally used by `host_machine_base`
and `guest_machine_base`.

### `admin_users`

Setup admin user accounts & SSH keys.

- `create_jump_user`: if a system user `jump` for use as an SSH jump host shall
  be created with all keys with non-empty `realms`
- `realm`: task for which user accounts should be created (effectively limits
  access to the machine this gets deployed to)
- `admin_users`: list of user objects, containing the following fields:
  - `name`: username used for login
  - `comment`: real name of the user
  - `password`: optional hashed password of the user (will only be used if the
    user doesn't exist already); use `mkpasswd -m sha512crypt` to create a hash
  - `keys`: list of SSH public keys, must be `sk-...` keys
    (backed by a hardware key)
  - `realms`: list of tasks this admin has the permission to administrate;
    currently the following values are available:
    - `core`: root access to the host system

### `host_machine_base`

Setup of a new host server, that can then run LXC containers.

This also installs LXC and **sets up the LXC DNS server to respond to
`<container>.lxc.local`**. It also configures HAProxy & LEGO, heavily using
templates & variables for different use cases.

- `bond_network`: boolean value to determine if all `enp*` interfaces should be
  bonded via LACP as a `bond0` interface
- `network_interface`: specify an interface for internet access; defaults to
  `bond0` or `eno1` depending on `bond_network`
- `network_addresses`: array of IPv4/IPv6 addresses to assign to
  `network_interface`; if empty, use DHCP
- `network_gateways`: array of IPv4/IPv6 gateway addresses to use with
  `network_interface`
- `dns_servers`: array of IPv4/IPv6 DNS servers to use for name resolution
- `realm`: see `admin_users` role
- `haproxy_debug`: if true, HAProxy will print out more debug information
- Secrets:
  - `acme_email`: email address to use for an ACME account
  - `acme_method`: LEGO arguments for the ACME method, e.g. `--dns <provider>`
  - `acme_dns_settings`: environment variables for the LEGO DNS configuration

### `guest_container`

Setup a new LXC container on the host system.

- `container_name`: name of the LXC container to create
- `realm`: see `realms` role

### `guest_machine_base`

Setup the inside of a new LXC container.

### `distributed-storage`

(TODO) Ceph configuration

### `gitea`

(TODO) Gitea with database & cache

### `pages`

(TODO) Codeberg Pages

### `badges`

(TODO) Codeberg Badges

### `mailserver`

(TODO) Mailserver configuration, eventually using Docker?!

### `association`

(TODO) Registration Server, Voting Tool, etc.

### `monitoring`

(TODO) Monitoring & log server deployment

### `backup`

(TODO) Backup server deployment
